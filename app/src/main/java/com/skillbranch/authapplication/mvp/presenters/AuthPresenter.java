package com.skillbranch.authapplication.mvp.presenters;

import android.support.annotation.Nullable;

import com.skillbranch.authapplication.data.managers.DataManager;
import com.skillbranch.authapplication.data.managers.PreferencesManager;
import com.skillbranch.authapplication.mvp.models.AuthModel;
import com.skillbranch.authapplication.mvp.views.IAuthView;
import com.skillbranch.authapplication.ui.custom_views.AuthPanel;

public class AuthPresenter implements IAuthPresenter{

    /**
     * Паттерн синглтон
     */
    private static AuthPresenter ourInstance = new AuthPresenter();

    /**
     * Модель авторизации
     */
    private AuthModel mAuthModel;

    /**
     * View авторизации
     */
    private IAuthView mAuthView;

    /**
     * Конструктор
     */
    public AuthPresenter() {
        mAuthModel = new AuthModel();
    }

    /**
     * Возвращаем сами себя
     * @return
     */
    public static AuthPresenter getInstance(){
        return ourInstance;
    }

    @Override
    public void takeView(IAuthView authView) {
        mAuthView = authView;
    }

    @Override
    public void dropView() {
        mAuthView = null;
    }

    @Override
    public void initView() {
        if (getView()!=null) {
            if (checkUserAuth()) {
                getView().hideLoginBtn();
            } else {
                getView().showLoginBtn();
            }
        }
    }

    @Nullable
    @Override
    public IAuthView getView() {
        return mAuthView;
    }

    @Override
    public void clickOnLogin() {
        if (getView()!=null  && getView().getAuthPanel()!=null){
            getView().hideLoad();
            if (getView().getAuthPanel().isIdle()){
                getView().getAuthPanel().setCustomState(AuthPanel.LOGIN_STATE);
            }
            else {
                if (!getView().getAuthPanel().checkUserMailInput() || !getView().getAuthPanel().checkUserPassInput()){
                    if (!getView().getAuthPanel().checkUserMailInput()){
                        getView().showMessage("No email input");
                    }
                    else if(!getView().getAuthPanel().checkUserPassInput()){
                        getView().showMessage("No pass input");
                    }
                }
                else {
                    String userToken = getView().getAuthPanel().getUserEmail();
                    String userPassToken = getView().getAuthPanel().getUserPass();
                    DataManager manager = DataManager.getINSTANCE();
                    if (manager.getPreferencesManager().getAuthToken()!=null){
                        if (!manager.getPreferencesManager().checkAuthToken(userToken)){
                            getView().showMessage("Введенный email не совпадает!");
                        }
                        else {
                            if (manager.getPreferencesManager().getAuthPassToken()!=null){
                                if (!manager.getPreferencesManager().checkAuthPassToken(userPassToken)){
                                    getView().showMessage("Введенный pass не совпадает!");
                                }
                            }
                        }
                    }
                    mAuthModel.loginUser(userToken, userPassToken);
                    getView().showMessage("request for user auth");
                    manager.getPreferencesManager().saveAuthToken(userToken);
                    manager.getPreferencesManager().saveAuthPassToken(userPassToken);
                }
            }
        }
    }

    @Override
    public void clickOnFb() {
        if (getView()!=null){
            getView().showMessage("Facebook");
        }
    }

    @Override
    public void clickOnTwitter() {
        if (getView()!=null){
            getView().showMessage("Twitter");
        }
    }

    @Override
    public void clickOnVk() {
        if (getView()!=null){
            getView().showMessage("VK");
        }
    }

    @Override
    public void clickOnShowCatalog() {
        if (getView()!=null){
            getView().showLoad();
            getView().showMessage("Показать каталог");
        }
    }

    @Override
    public boolean checkUserAuth() {
        return mAuthModel.isAuthUser();
    }
}
