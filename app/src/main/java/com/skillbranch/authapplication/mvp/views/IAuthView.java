package com.skillbranch.authapplication.mvp.views;

import android.support.annotation.Nullable;

import com.skillbranch.authapplication.mvp.presenters.IAuthPresenter;
import com.skillbranch.authapplication.ui.custom_views.AuthPanel;

/**
 * Абстракция реализующая набор методов представления для авторизации
 */
public interface IAuthView {
    /**
     * Показать сообщение
     * @param message сообщение
     */
    void showMessage(String message);

    /**
     * Показать ошибку
     * @param e - ошибка
     */
    void showError(Throwable e);

    /**
     * Показать загрузку
     */
    void showLoad();

    /**
     * Скрыть загрузку
     */
    void hideLoad();

    /**
     * Presenter
     * @return
     */
    IAuthPresenter getPresenter();

    /**
     * Показать Кнопку логин
     */
    void showLoginBtn();

    /**
     * Скрыть Кнопку логин
     */
    void hideLoginBtn();

    /**
     * Новая панель авторизации
     * @return
     */
    @Nullable
    AuthPanel getAuthPanel();
}
