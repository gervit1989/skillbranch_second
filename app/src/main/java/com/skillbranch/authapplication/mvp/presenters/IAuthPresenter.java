package com.skillbranch.authapplication.mvp.presenters;

import android.support.annotation.Nullable;

import com.skillbranch.authapplication.mvp.views.IAuthView;

/**
 * Абстракция над бизнес логикой связанной с представлением и взаимодействием с данными.
 * Presenter подписывается на события UI и изменяет данные в модели по запросу. Содержит
 * ссылку на экземпляр модели и экземпляр представления
 */
public interface IAuthPresenter {

    /**
     *  проинициализировать экземпляр View
     */
    void takeView(IAuthView authView);

    /**
     *  отвязать  экземпляр View
     */
    void dropView();

    /**
     *  проинициализировать состояние View (onLoad)
     */
    void initView();

    @Nullable
    IAuthView getView();

    /**
     * обработка щелчка на кнопку "показать каталог"
     */
    void clickOnLogin();

    /**
     * обработка щелчка на кнопку "Facebook"
     */
    void clickOnFb();

    /**
     * обработка щелчка на кнопку "Twitter"
     */
    void clickOnTwitter();

    /**
     * обработка щелчка на кнопку "VK"
     */
    void clickOnVk();

    /**
     * обработка щелчка на кнопку "показать каталог"
     */
    void clickOnShowCatalog();

    /**
     *  проверить есть ли сохраненный токен пользователя (авторизован ли пользователь)
     * @return Да или нет
     */
    boolean checkUserAuth();
}
