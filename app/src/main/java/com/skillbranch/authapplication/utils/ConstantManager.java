package com.skillbranch.authapplication.utils;

/**
 * Created by Zver on 23.10.2016.
 */

/**
 * Хранилище констант
 */
public interface ConstantManager {

    boolean IS_CUSTOM_ALL = false;
    boolean IS_CUSTOM_BOOK_FONT = false;
    String REG_USER_LOGIN_KEY ="USER_AUTH_LOGIN";
    String REG_USER_PASS_KEY="USER_PASS";
}
