package com.skillbranch.authapplication.data.managers;

import android.content.Context;
import android.graphics.Typeface;

import com.skillbranch.authapplication.utils.SkillBranchAuthApplication;


public class DataManager {
    /**
     * Отслеживание
     */
    private static final String TAG = "DataManager";

    /**
     * Экземпляр класса
     */
    private static DataManager INSTANCE = null;

    /**
     * Доступ к пользовательским значениям
     */
    private PreferencesManager mPreferencesManager;

    /**
     * Контекст приложения
     */
    private Context mContext;
    private static Typeface fontRegular;
    private static Typeface fontBook;

    /**
     * Конструктор
     */
    public DataManager() {
        //- Сохранение настроек приложения
        this.mPreferencesManager = new PreferencesManager();

        //- Контекст приложения
        this.mContext = SkillBranchAuthApplication.getContext();
        this.fontBook = SkillBranchAuthApplication.getCustomBookFont();
        this.fontRegular = SkillBranchAuthApplication.getCustomRegularFont();
    }


    /**
     * Получение единственного экземпляра
     * @return Менеджер
     */
    public static DataManager getINSTANCE(){
        if (INSTANCE==null){
            INSTANCE = new DataManager();
        }
        return  INSTANCE;
    }

    /**
     * Контекст приложения
     * @return контекст
     */
    public Context getContext() {
        return mContext;
    }

    /**
     * Доступ к пользовательским значениям
     * @return Менеджер пользовательских значений
     */
    public PreferencesManager getPreferencesManager() {
        return mPreferencesManager;
    }


    public static Typeface getCustomRegularFont() {
        return fontRegular;
    }

    public static Typeface getCustomBookFont() {
        return fontBook;
    }
}
